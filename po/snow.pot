# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Enlightenment development team
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: enlightenment-devel@lists.sourceforge.net\n"
"POT-Creation-Date: 2017-12-16 21:56+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/e_mod_config.c:42
msgid "Snow Configuration"
msgstr ""

#: src/e_mod_config.c:99
msgid "General Settings"
msgstr ""

#: src/e_mod_config.c:100
msgid "Show Trees"
msgstr ""

#: src/e_mod_config.c:104
msgid "Snow Density"
msgstr ""

#: src/e_mod_config.c:106
msgid "Sparse"
msgstr ""

#: src/e_mod_config.c:108
msgid "Medium"
msgstr ""

#: src/e_mod_config.c:110
msgid "Dense"
msgstr ""

#: src/e_mod_main.c:33
msgid "Look"
msgstr ""

#: src/e_mod_main.c:35
msgid "Snow"
msgstr ""
